package sample;

/*
    Данный класс описывает ячейки, по которым будем стрелять. В ячейке будет иконка.
    В ячейке может быть корабль а может и не быть!
    */

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Jach extends ImageView {
    int hight = 50;//высота ячейки +
    int weigth = 50;//ширина ячейки +
    boolean ship;//есть ли в ячейке корабль -
    int stroke;//нахождение ячейки по строке в массиве
    int stolb;//нахождение ячейки по столбцу в массиве
    Image imageDefault;//картинка на ячейке по умолчанию
    Image imagePopal;//картинка на ячейке при попадании
    Image imageMimo;//картинка на ячейке если промазал
    /*int m = 0;//счетчик хеш-кодов
    int [] arr = new int[25];//тут храняться все хеш-коды кнопок*/

    public Jach(boolean ship,
                int stroke,
                int stolb){
        this.ship = ship;
        this.stroke = stroke;
        this.stolb = stolb;
    }

    public Jach() {
    }

    public ImageView imageViewGenerator(){
        /*
        Данный метод будет создавать кнопки
        */
        Image imageDefault = new Image("ImageVievDefault.png");//картинка

        ImageView imageViewDefault = new ImageView(imageDefault);//добавляем картинку в ИмаджВью
        imageViewDefault.setFitWidth(weigth);//ширина картинки
        imageViewDefault.setFitHeight(hight);//высота картинки
        imageViewDefault.setVisible(true);
        /*
        Делаем для кнопки слушатель событий по нажатию мышки
        */
        imageViewDefault.setOnMouseClicked(event -> {
            System.out.println("Нажимается ячейка - " + imageViewDefault.getLayoutX());
            System.out.println("Нажимается ячейка - " + imageViewDefault.getLayoutY());
            Controller controller = new Controller();
            int pop = controller.clickJach();

            if (1 == 1){
                Image image = new Image("sample/ImageViexVzriv.png");
                imageViewDefault.setImage(image);
                imageViewDefault.setFitWidth(weigth);//ширина картинки
                imageViewDefault.setFitHeight(hight);//высота картинки
                imageViewDefault.setVisible(true);
            }
        });

        return imageViewDefault;
    }
}
