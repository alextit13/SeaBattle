package sample;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        VBox root = new VBox(10);//основное окною Сверху панель с кнопкой и текстФилд
        Scene scene = new Scene(root,900,400);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Морской бой");
        primaryStage.setResizable(false);//неизменяемый размер окна
        HBox hBox = new HBox(5);//Бокс для кнопки, текстфилда с состоянием
        hBox.setPrefHeight(50);//высота бокса для кнопки
        root.getChildren().add(hBox);//добавляем бокс для кнопки в основную панель

        Button buttonNewGame = new Button("Новая игра");
        buttonNewGame.setTranslateX(10);
        buttonNewGame.setTranslateY(10);
        buttonNewGame.setOnAction(event -> { //Делаем слушатель для кнопки Новая игра
            Controller controller = new Controller();//кнопка будет вызывать метод Контроллера
            controller.clickButtonNewGame();
        });
        hBox.getChildren().add(buttonNewGame);//обавляем кнопку на панель для кнопки

        HBox hBoxBattleField = new HBox(10);//Поле, на котором будут раполагаться поля для сражения
        root.getChildren().add(hBoxBattleField);

        GridPane gridPaneOne = new GridPane();//Поле по которому будем стрелять ЛЕВОЕ
        gridPaneOne.setPadding(new Insets(5,5,5,5));//внутренние отступы левой панели
        //gridPaneOne.setGridLinesVisible(true);//видимые полоски(ПОТОМ УБРАТЬ)
        gridPaneOne.setTranslateX(10);
        gridPaneOne.setTranslateY(10);
        gridPaneOne.setAlignment(Pos.CENTER);
        gridPaneOne.setHgap(10);//отытупы между внутренними элементами
        gridPaneOne.setVgap(10);//отытупы между внутренними элементами
        hBoxBattleField.getChildren().add(gridPaneOne);//Добавляем правое поле на ХБОКС

        //Хреновина ниже добавляет элементы из других классов на наши панельки.
        for (int i = 0; i < 5; i++) {
            for (int k = 0; k < 5; k++) {
                Jach j = new Jach(false,i,k);
                gridPaneOne.add(j.imageViewGenerator(),k,i);//добавили картинки (кнопки) на
                                                            // вражескую панельку
            }
        }
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
